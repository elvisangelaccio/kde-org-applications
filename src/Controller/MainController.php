<?php
/**
 * SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace App\Controller;

use App\Model\AppData;
use App\Providers\ApplicationsProviderFile;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

function startsWith($haystack, $needle)
{
     $length = strlen($needle);
     return (substr($haystack, 0, $length) === $needle);
}

/**
 * Class MainController
 * @package App\Controller
 */
class MainController extends AbstractController
{
    /**
     * @Route("/{_locale}", name="app_main_index", requirements={"_locale": "%app.locales%"}, defaults={"_locale": "en"})
     * @param Request $request
     * @param string $_locale
     * @return Response
     */
    public function index(Request $request, string $_locale): Response
    {
        if ($request->query->get('site_locale')) {
            return $this->redirectToRoute('app_main_index', ['_locale' => $request->query->get('site_locale')]);
        }
        $applicationsProvider = new ApplicationsProviderFile("index.json");

        return $this->render('main/index.html.twig', [
            'categories' => $applicationsProvider->getAllCategories(),
            'locale' => $_locale,
            'translations' => $this->getParameter('app.translations'),
        ]);
    }

    /**
     * @Route("/{category}", defaults={"_locale": "en"})
     * @Route("/{_locale}/{category}", name="app_main_category_intl", requirements={"_locale": "%app.locales%"})
     * @param Request $request
     * @param string $_locale
     * @param string $category
     * @return Response
     */
    public function category(Request $request, string $_locale, string $category): Response
    {
        if ($request->query->get('site_locale')) {
            return $this->redirectToRoute('app_main_category_intl', ['_locale' => $request->query->get('site_locale'), 'category' => $category]);
        }
        $applicationsProvider = new ApplicationsProviderFile("index.json");
        $categoryObject = $applicationsProvider->getApplicationsByCategory(ucfirst($category));

        if ($categoryObject) {
            return $this->render('main/category.html.twig', [
                'category' => $categoryObject,
                'locale' => $_locale,
                'translations' => $this->getParameter('app.translations'),
            ]);
        }
        $applicationObj = AppData::fromBinary($category);
        if ($applicationObj === null) {
            return new Response(200, $category);
        }
        return $this->redirectToRoute('app_main_application_intl', [
            '_locale' => $_locale,
            'category' => strtolower($applicationObj->getPrimaryCategory()),
            'application' => $applicationObj->getId(),
            'translations' => $this->getParameter('app.translations'),
        ]);
    }

    /**
     * @Route("/{category}/{application}", requirements={"_locale": "%app.locales%"}, defaults={"_locale": "en"})
     * @Route("/{_locale}/{category}/{application}", name="app_main_application_intl", requirements={"_locale": "%app.locales%"})
     * @param Request $request
     * @param string $_locale
     * @param string $category
     * @param string $application
     * @return Response
     */
    public function application(Request $request, string $_locale, string $category, string $application): Response
    {
        if ($request->query->get('site_locale')) {
            return $this->redirectToRoute('app_main_application_intl', ['_locale' => $request->query->get('site_locale'), 'category' => $category, 'application' => $application]);
        }

        $applicationObj = AppData::fromName($application);

        if (!($applicationObj || startsWith($application, 'org'))) {
            return $this->redirectToRoute('app_main_application_intl', ['_locale' => $_locale, 'category' => $category, 'application' => 'org.kde.' . $application]);
        } else if (!$applicationObj) {
            throw $this->createNotFoundException('The product does not exist');
        }

        return $this->render('main/application.html.twig', [
            'application' => $applicationObj,
            'category' => $category,
            'locale' => $_locale,
            'translations' => $this->getParameter('app.translations'),
        ]);
    }
}
