<?php
// SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
//
// SPDX-License-Identifier: CC0-1.0

// Generate locale requirement
\Locale::setDefault('en');

$localeRequirements = [];

foreach (\ResourceBundle::getLocales('') as $name) {
  $localeRequirements[] = $name;
}

$container->setParameter('app.locales', implode('|', $localeRequirements));


$container->setParameter('app.translations', [
"ca",
"ca@valencia",
"cs",
"es",
"eu",
"fr",
"it",
"nl",
"nn",
"pt_BR",
"pt",
"ru",
"sk",
"sl",
"uk",
]);
